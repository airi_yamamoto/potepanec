# Store
Spree::Store.create!({
  name: "BIGBAGstore",
  url: "bigbagstore.com",
  mail_from_address: "bigbag@example.com",
  code: "bigbag",
})

# Country
Spree::Country.create!(name: "Japan", iso_name: "JAPAN", states_required: true)

# State in Japan
country = Spree::Country.find_by(name: "Japan")

Spree::State.create!(name: "Hokkaido", country: country)
Spree::State.create!(name: "Aomori", country: country)
Spree::State.create!(name: "Iwate", country: country)
Spree::State.create!(name: "Miyagi", country: country)
Spree::State.create!(name: "Akita", country: country)
Spree::State.create!(name: "Yamagata", country: country)
Spree::State.create!(name: "Fukushima", country: country)
Spree::State.create!(name: "Ibaraki", country: country)
Spree::State.create!(name: "Tochigi", country: country)
Spree::State.create!(name: "Gunma", country: country)
Spree::State.create!(name: "Saitama", country: country)
Spree::State.create!(name: "Chiba", country: country)
Spree::State.create!(name: "Tokyo", country: country)
Spree::State.create!(name: "Kanagawa", country: country)
Spree::State.create!(name: "Niigata", country: country)
Spree::State.create!(name: "Toyama", country: country)
Spree::State.create!(name: "Ishikawa", country: country)
Spree::State.create!(name: "Fukui", country: country)
Spree::State.create!(name: "Yamanashi", country: country)
Spree::State.create!(name: "Nagano", country: country)
Spree::State.create!(name: "Gifu", country: country)
Spree::State.create!(name: "Shizuoka", country: country)
Spree::State.create!(name: "Aichi", country: country)
Spree::State.create!(name: "Mie", country: country)
Spree::State.create!(name: "Shiga", country: country)
Spree::State.create!(name: "Kyoto", country: country)
Spree::State.create!(name: "Osaka", country: country)
Spree::State.create!(name: "Hyogo", country: country)
Spree::State.create!(name: "Nara", country: country)
Spree::State.create!(name: "Wakayama", country: country)
Spree::State.create!(name: "Tottori", country: country)
Spree::State.create!(name: "Shimane", country: country)
Spree::State.create!(name: "Okayama", country: country)
Spree::State.create!(name: "Hiroshima", country: country)
Spree::State.create!(name: "Yamaguchi", country: country)
Spree::State.create!(name: "Tokushima", country: country)
Spree::State.create!(name: "Kagawa", country: country)
Spree::State.create!(name: "Ehime", country: country)
Spree::State.create!(name: "Kochi", country: country)
Spree::State.create!(name: "Fukuoka", country: country)
Spree::State.create!(name: "Saga", country: country)
Spree::State.create!(name: "Nagasaki", country: country)
Spree::State.create!(name: "Kumamoto", country: country)
Spree::State.create!(name: "Oita", country: country)
Spree::State.create!(name: "Miyazaki", country: country)
Spree::State.create!(name: "Kagoshima", country: country)
Spree::State.create!(name: "Okinawa", country: country)

# Zone
Spree::Zone.create!(name: "Japan", description: "default")
Spree::Zone.find_by(name: "Japan").zone_members.create!(zoneable_type: "Spree::Country", zoneable: country)

# PaymentMethod
Spree::PaymentMethod::BogusCreditCard.create!(name: "Credit Card", description: "Pay by credit card", active: true)

# StockLocation
Spree::StockLocation.create!(name: 'default', active: true, country: country)

# StockMovement
Spree::Variant.all.each do |variant|
  Spree::StockMovement.create!(quantity: 200, stock_item: variant.stock_items.first)
end
