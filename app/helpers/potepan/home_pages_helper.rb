module Potepan::HomePagesHelper
  def go_to_products_page
    if Spree::Taxon.first.present?
      potepan_category_path(Spree::Taxon.first.id)
    else
      "#"
    end
  end
end
