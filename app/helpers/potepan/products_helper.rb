module Potepan::ProductsHelper
  def products_count_for_option(value:, category:)
    Spree::Product.includes(variants: :option_values).
      in_taxon(category).
      where(spree_option_values: { presentation: value.presentation }).count
  end
end
