class Potepan::OrderMailer < ApplicationMailer
  def confirm_email(order)
    @order = order
    @store = order.store
    @address = order.ship_address
    subject = "ご注文ありがとうございます！(注文番号:#{@order.number})"

    mail to: @order.email, from: "#{@store.name}<#{@store.mail_from_address}>", subject: subject
  end
end
