module Potepan::OrderDecorator
  def previous_state
    case state
    when "address"
      "cart"
    when "payment"
      "address"
    when "confirm"
      "payment"
    end
  end

  def payment_status_design
    case state
    when "address"
      "disabled"
    when "payment"
      "active"
    when "confirm"
      "complete fullBar"
    end
  end

  def pay_by_credit_card
    available_payment_methods.find_by(name: "Credit Card")
  end

  def insufficient_stock_line_items
    line_items.includes(:variant).select(&:insufficient_stock?)
  end

  Spree::Order.prepend self
end
