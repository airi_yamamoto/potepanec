module Potepan::TaxonDecorator
  POPULAR_TAXONS = %w(bags categories t-shirts).freeze

  def self.prepended(base)
    def base.three_popular_taxons
      where(name: POPULAR_TAXONS).order(:name)
    end
  end

  Spree::Taxon.prepend self
end
