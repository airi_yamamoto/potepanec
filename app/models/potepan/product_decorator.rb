module Potepan::ProductDecorator
  def related_products
    Spree::Product.in_taxons(taxons).distinct.
      where.not(id: id).
      includes(master: [:default_price, :images])
  end

  def image_pickup
    images.first || Spree::Image.new
  end

  def self.prepended(base)
    base.scope :desc_by_available, -> do
      base.includes(master: [:default_price, :images]).
        order(available_on: :desc).
        where("available_on <= ?", Time.current)
    end

    base.scope :with_sort_by, -> (sort) do
      return if sort.blank?
      case sort
      when "NEW_PRODUCT"
        base.order(available_on: :desc)
      when "OLD_PRODUCT"
        base.order(available_on: :asc)
      when "HIGH_PRICE"
        base.descend_by_master_price
      when "LOW_PRICE"
        base.ascend_by_master_price
      end
    end

    base.scope :filter_by_value, -> (value) do
      return if value.blank?
      base.joins(variants: :option_values).
        where(spree_option_values: { presentation: value })
    end

    base.scope :search_by, -> (word) do
      base.includes(master: [:default_price, :images]).
        where("name LIKE :word OR description LIKE :word", word: "%#{sanitize_sql_like(word)}%")
    end

    def base.with_filter_and_sort(filter:, sort:)
      filter_by_value(filter).with_sort_by(sort).uniq
    end
  end

  Spree::Product.prepend self
end
