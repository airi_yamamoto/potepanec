module Potepan::LineItemDecorator
  def image_pickup
    variant.gallery.images.first || product.image_pickup
  end

  Spree::LineItem.prepend self
end
