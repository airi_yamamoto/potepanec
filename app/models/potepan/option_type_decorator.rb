module Potepan::OptionTypeDecorator
  def self.prepended(base)
    base.scope :colors, -> { find_by(presentation: "Color").option_values }
    base.scope :sizes,  -> { find_by(presentation: "Size").option_values }
  end

  Spree::OptionType.prepend self
end
