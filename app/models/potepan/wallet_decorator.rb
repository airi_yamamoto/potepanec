module Potepan::WalletDecorator
  def payment_source_of_wallet
    user.wallet_payment_sources.includes(:payment_source)
  end

  Spree::Wallet.prepend self
end
