class Potepan::ProductsController < ApplicationController
  NUMBERS_OF_RELATED_PRODUCTS = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.sample(NUMBERS_OF_RELATED_PRODUCTS)
  end

  def search
    @search_word = params[:search]
    @products = Spree::Product.search_by(@search_word)
  end
end
