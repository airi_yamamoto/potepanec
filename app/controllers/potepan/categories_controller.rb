class Potepan::CategoriesController < ApplicationController
  def show
    @category = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.includes(taxons: :products)
    @option_colors = Spree::OptionType.colors
    @option_sizes = Spree::OptionType.sizes

    @products = @category.
      all_products.includes(master: [:default_price, :images]).
      with_filter_and_sort(filter: params[:value], sort: params[:sort])
  end
end
