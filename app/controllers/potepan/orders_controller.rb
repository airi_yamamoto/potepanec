class Potepan::OrdersController < ApplicationController
  before_action :assign_order, only: :update
  rescue_from ActiveModel::RangeError, with: :rescue_from_so_many_quantity

  def show
    @order = Spree::Order.find_by!(number: params[:id])
    authorize! :read, @order, cookies.signed[:guest_token]
  end

  def edit
    @order = current_order || Spree::Order.incomplete.
      find_or_initialize_by(guest_token: cookies.signed[:guest_token])
    @line_items = @order.line_items.includes(variant: [:images, :product])
    authorize! :read, @order, cookies.signed[:guest_token]
    associate_user
  end

  def populate
    @order = current_order(create_order_if_necessary: true)
    authorize! :update, @order, cookies.signed[:guest_token]

    variant  = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].present? ? params[:quantity].to_i : 1

    if !quantity.between?(1, 100)
      @order.errors.add(:base, t('spree.please_enter_reasonable_quantity'))
    end

    begin
      @line_item = @order.contents.add(variant, quantity)
    rescue ActiveRecord::RecordInvalid => error
      @order.errors.add(:base, error.record.errors.full_messages.join(", "))
    end

    if @order.errors.any?
      flash[:error] = @order.errors.full_messages.join(", ")
      redirect_back_or_default(potepan_root_path)
      return
    else
      redirect_to potepan_cart_path
    end
  end

  def update
    authorize! :update, @order, cookies.signed[:guest_token]
    if @order.contents.update_cart(order_params)
      if params.key?(:checkout)
        @order.next if @order.cart?
        redirect_to potepan_checkout_state_path(@order.state)
        return
      end
    end
    redirect_to potepan_cart_path
  end

  private

  def order_params
    params.require(:order).permit(line_items_attributes: [:id, :quantity])
  end

  def assign_order
    @order = current_order
    unless @order
      flash[:error] = "Order not found."
      redirect_to potepan_root_path
      return
    end
  end

  def rescue_from_so_many_quantity
    flash[:error] = "Quantity is so big. The quantity should be less than 100 per product."
    redirect_back(fallback_location: potepan_cart_path)
  end
end
