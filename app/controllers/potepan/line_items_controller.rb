class Potepan::LineItemsController < ApplicationController
  def destroy
    line_item = Spree::LineItem.find_by(id: params[:id])
    line_item.destroy if line_item.present?
    redirect_back(fallback_location: potepan_root_path)
  end
end
