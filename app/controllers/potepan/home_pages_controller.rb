class Potepan::HomePagesController < ApplicationController
  NEW_PRODUCTS_LIMIT = 8

  def index
    @new_products = Spree::Product.desc_by_available.limit(NEW_PRODUCTS_LIMIT)
    @categories = Spree::Taxon.three_popular_taxons
  end
end
