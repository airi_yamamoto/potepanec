class Potepan::CheckoutController < ApplicationController
  before_action :load_order
  before_action :set_state_if_present

  before_action :ensure_order_not_completed
  before_action :ensure_checkout_allowed
  before_action :ensure_sufficient_stock_lines
  before_action :ensure_valid_state

  before_action :associate_user
  before_action :check_authorization

  before_action :setup_for_current_state, only: [:edit, :update]

  rescue_from Spree::Core::GatewayError, with: :rescue_from_spree_gateway_error
  rescue_from Spree::Order::InsufficientStock, with: :insufficient_stock_error

  def update
    if update_order
      unless transition_forward
        redirect_on_failure
        return
      end
      if @order.completed?
        finalize_order
      else
        send_to_next_state
      end
    else
      render :edit
    end
  end

  private

  def update_order
    Spree::OrderUpdateAttributes.new(@order, update_params, request_env: request.headers.env).apply
  end

  def redirect_on_failure
    flash[:error] = @order.errors.full_messages.join("\n")
    redirect_to(potepan_checkout_state_path(@order.state))
  end

  def transition_forward
    if @order.can_complete?
      @order.complete
    else
      @order.next
    end
  end

  def finalize_order
    @current_order = nil
    set_successful_flash_notice
    redirect_to completion_route
  end

  def set_successful_flash_notice
    flash.notice = t('spree.order_processed_successfully')
  end

  def send_to_next_state
    @order.next if @order.state == "delivery"
    redirect_to potepan_checkout_state_path(@order.state)
  end

  def update_params
    update_params = massaged_params[:order]
    if update_params.present?
      update_params.permit(permitted_checkout_attributes)
    else
      # currently allow update requests without any parameters in them
      {}
    end
  end

  def massaged_params
    massaged_params = params.deep_dup

    move_payment_source_into_payments_attributes(massaged_params)
    if massaged_params[:order] && massaged_params[:order][:existing_card].present?
      move_existing_card_into_payments_attributes(massaged_params)
    end
    move_wallet_payment_source_id_into_payments_attributes(massaged_params)
    set_payment_parameters_amount(massaged_params, @order)

    massaged_params
  end

  # should be overriden if you add new checkout_step that don't match solidus checkout_steps
  def skip_state_validation?
    false
  end

  def load_order
    @order = current_order
    redirect_to(potepan_cart_path) && return unless @order
  end

  # go to next step if params[:state]
  def set_state_if_present
    if params[:state]
      if @order.can_go_to_state?(params[:state]) && !skip_state_validation?
        redirect_to potepan_checkout_state_path(@order.state)
      end
      @order.state = params[:state]
    end
  end

  # check the order contains line_items
  def ensure_checkout_allowed
    unless @order.checkout_allowed?
      redirect_to potepan_cart_path
    end
  end

  # check the order is not completed yet
  def ensure_order_not_completed
    redirect_to potepan_cart_path if @order.completed?
  end

  # check the order's line_items have enough stock
  def ensure_sufficient_stock_lines
    if @order.insufficient_stock_line_items.present?
      out_of_stock_items = @order.insufficient_stock_line_items.collect(&:name).to_sentence
      flash[:error] =
        t('spree.inventory_error_flash_for_insufficient_quantity', names: out_of_stock_items)
      redirect_to potepan_cart_path
    end
  end

  # check if the order can go to checkout step
  def ensure_valid_state
    unless skip_state_validation?
      if (params[:state] && !@order.has_checkout_step?(params[:state])) ||
        (!params[:state] && !@order.has_checkout_step?(@order.state))
        @order.state = 'cart'
        redirect_to potepan_cart_path
      end
    end
  end

  def completion_route
    potepan_order_path(@order.number)
  end

  def setup_for_current_state
    method_name = :"before_#{@order.state}"
    send(method_name) if respond_to?(method_name, true)
  end

  # set the default country for filing address (default: Japan)
  def before_address
    @order.assign_default_user_addresses
    default = { country: Spree::Country.find_by(name: "Japan") }
    @order.build_ship_address(default) unless @order.ship_address
  end

  def before_delivery
    return if params[:order].present?

    packages = @order.shipments.map(&:to_package)
    @differentiator = Spree::Stock::Differentiator.new(@order, packages)
  end

  def before_payment
    if @order.checkout_steps.include? "delivery"
      packages = @order.shipments.map(&:to_package)
      @differentiator = Spree::Stock::Differentiator.new(@order, packages)
      @differentiator.missing.each do |variant, quantity|
        @order.contents.remove(variant, quantity)
      end
    end

    if try_spree_current_user && try_spree_current_user.respond_to?(:wallet)
      @wallet_payment_sources = try_spree_current_user.wallet.wallet_payment_sources
      @default_wallet_payment_source = @wallet_payment_sources.detect(&:default) ||
                                      @wallet_payment_sources.first

      @payment_sources = try_spree_current_user.
        wallet.payment_source_of_wallet.
        map(&:payment_source).
        select { |ps| ps.is_a?(Spree::CreditCard) }
    end
  end

  def rescue_from_spree_gateway_error(exception)
    flash.now[:error] = t('spree.spree_gateway_error_flash_for_checkout')
    @order.errors.add(:base, exception.message)
    render :edit
  end

  def check_authorization
    authorize!(:edit, current_order, cookies.signed[:guest_token])
  end

  def insufficient_stock_error
    packages = @order.shipments.map(&:to_package)
    if packages.empty?
      flash[:error] = I18n.t('spree.insufficient_stock_for_order')
      redirect_to cart_path
    else
      availability_validator = Spree::Stock::AvailabilityValidator.new
      unavailable_items = @order.line_items.
        reject { |line_item| availability_validator.validate(line_item) }
      if unavailable_items.any?
        item_names = unavailable_items.map(&:name).to_sentence
        flash[:error] =
          t('spree.inventory_error_flash_for_insufficient_shipment_quantity',
            unavailable_items: item_names)
        @order.restart_checkout_flow
        redirect_to spree.checkout_state_path(@order.state)
      end
    end
  end

  def deprecated_payment_sources
    try_spree_current_user.wallet.payment_source_of_wallet.
      map(&:payment_source).
      select { |ps| ps.is_a?(Spree::CreditCard) }
  end
end
