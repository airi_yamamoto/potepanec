Rails.application.routes.draw do
  # This line mounts Solidus's routes at the root of your application.
  # This means, any requests to URLs such as /products, will go to Spree::ProductsController.
  # If you would like to change where this engine is mounted, simply change the :at option to something different.
  #
  # We ask that you don't use the :as option here, as Solidus relies on it being the default of "spree"
  mount Spree::Core::Engine, at: '/'

  namespace :potepan do
    root to: 'home_pages#index'

    get   :cart,     to: "orders#edit",     as: :cart
    patch :cart,     to: "orders#update",   as: :update_cart
    post  :populate, to: "orders#populate", as: :populate

    patch '/checkout/update/:state', to: 'checkout#update', as: :update_checkout
    get   '/checkout/:state',        to: 'checkout#edit',   as: :checkout_state
    get   '/checkout',               to: 'checkout#edit',   as: :checkout

    resources :products, only: :show do
      get :search, on: :collection
    end

    resources :categories,          only: :show
    resources :orders,              only: [:show, :edit, :update]
    resources :line_items,          only: :destroy
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
