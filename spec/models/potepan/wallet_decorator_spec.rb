require 'rails_helper'

RSpec.describe Potepan::WalletDecorator, type: :model do
  describe "model method" do
    let(:user)        { create(:user) }
    let(:wallet)      { Spree::Wallet.new(user) }
    let(:credit_card) { create(:credit_card, user_id: user.id) }

    describe "payment_source_of_wallet" do
      let!(:wallet_credit_card) { wallet.add(credit_card) }

      it "return the wallet user's payment sources" do
        expect(wallet.payment_source_of_wallet).to eq [wallet_credit_card]
      end
    end
  end
end
