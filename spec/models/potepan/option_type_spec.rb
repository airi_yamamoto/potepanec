require 'rails_helper'

RSpec.describe Potepan::OptionTypeDecorator, type: :model do
  describe "model scope" do
    let!(:type_color) { create(:option_type, presentation: "Color") }
    let!(:value_red)  { create(:option_value, presentation: "Red", option_type: type_color) }
    let!(:type_size)  { create(:option_type, presentation: "Size") }
    let!(:value_size) { create(:option_value, presentation: "S", option_type: type_size) }

    describe "colors" do
      it "returns only values belongs to option Color" do
        expect(Spree::OptionType.colors).to eq [value_red]
      end
    end

    describe "sizes" do
      it "returns only values belongs to option Size" do
        expect(Spree::OptionType.sizes).to eq [value_size]
      end
    end
  end
end
