require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe "model method" do
    let!(:taxon_a)        { create(:taxon) }
    let!(:taxon_b)        { create(:taxon) }
    let!(:taxon_other)    { create(:taxon) }
    let!(:product)        { create(:product, taxons: [taxon_a, taxon_b]) }
    let!(:products_other) { create_list(:product, 2, name: "Others", taxons: [taxon_other]) }
    let!(:products) do
      create_list(:product, 2, name: "Related", taxons: [taxon_a, taxon_b])
      create_list(:product, 3, name: "Related", taxons: [taxon_b])
    end

    describe "related_products" do
      subject { product.related_products }

      it { is_expected.to include(*products) }
      it { is_expected.not_to include(*products_other) }
      it { is_expected.not_to include(product) }

      it "returns distinct products" do
        is_expected.to eq product.related_products.uniq
      end
    end

    describe "image_pickup" do
      let(:first_image)  { create(:image) }
      let(:second_image) { create(:image) }

      context "when product has no images" do
        it "returns Spree::Image.new" do
          expect(product.images.count).to eq 0
          expect(product.image_pickup.url(:product)).to eq "noimage/product.png"
        end
      end

      context "when product has images" do
        it "returns a product image" do
          product.images << first_image << second_image
          expect(product.image_pickup).to eq first_image
        end
      end
    end
  end

  describe "model scope" do
    describe "for testing filtering and sorting" do
      let(:products_all)  { Spree::Product.all }
      let!(:product_old)  { create(:product, available_on: 1.year.ago) }
      let!(:red_new)      { create(:product, available_on: Time.current) }
      let!(:blue_high)    { create(:product, price: "25", available_on: 1.week.ago) }
      let!(:size_low)     { create(:product, price: "15", available_on: 1.week.ago) }

      let!(:variant_red)  { create(:variant, product: red_new, option_values: [value_red]) }
      let!(:variant_blue) { create(:variant, product: blue_high, option_values: [value_blue]) }
      let!(:variant_size) { create(:variant, product: size_low, option_values: [value_size]) }

      let!(:value_red)    { create(:option_value, presentation: "Red") }
      let!(:value_blue)   { create(:option_value, presentation: "Blue") }
      let!(:value_size)   { create(:option_value, presentation: "S") }

      describe "for sorting" do
        describe "desc_by_available" do
          let!(:product_feature) { create(:product, available_on: 1.day.from_now) }

          it "returns new_product first" do
            expect(Spree::Product.first).to eq product_old
            expect(Spree::Product.desc_by_available.first).to eq red_new
          end

          it "does not return not available products" do
            expect(Spree::Product.desc_by_available).not_to include product_feature
          end
        end

        describe "with_sort_by" do
          subject { [sort_by_scope.first, sort_by_scope.last] }

          let(:sort_by_scope) { Spree::Product.with_sort_by(scope) }

          context "sort by 'NEW_PRODUCT'" do
            let(:scope) { "NEW_PRODUCT" }

            it "returns products from new to old" do
              is_expected.to eq [red_new, product_old]
            end
          end

          context "sort by 'OLD_PRODUCT'" do
            let(:scope) { "OLD_PRODUCT" }

            it "returns products from old to new" do
              is_expected.to eq [product_old, red_new]
            end
          end

          context "sort by 'HIGH_PRICE'" do
            let(:scope) { "HIGH_PRICE" }

            it "returns products from high_price to low_price" do
              is_expected.to eq [blue_high, size_low]
            end
          end

          context "sort by 'LOW_PRICE'" do
            let(:scope) { "LOW_PRICE" }

            it "returns products from low_price to high_price" do
              is_expected.to eq [size_low, blue_high]
            end
          end
        end
      end

      describe "for filtering" do
        describe "filter_by_value" do
          subject { filtered_by_value }

          let(:filtered_by_value) { Spree::Product.filter_by_value(value) }

          context "when value is nil" do
            let(:value) { nil }

            it "has all products" do
              is_expected.to include(*products_all)
            end
          end

          context "when value presents" do
            context "filter_by_red" do
              let(:value) { "Red" }

              it { is_expected.to eq [red_new] }
            end

            context "filter_by_blue" do
              let(:value) { "Blue" }

              it { is_expected.to eq [blue_high] }
            end

            context "filter_by_size" do
              let(:value) { "S" }

              it { is_expected.to eq [size_low] }
            end
          end
        end
      end

      describe "for testing 'with_filter_and_sort'" do
        subject { filtering_and_sorting }

        let(:filtering_and_sorting) do
          Spree::Product.with_filter_and_sort(filter: value, sort: sort)
        end

        context "when params is nil" do
          let(:value) { nil }
          let(:sort) { nil }

          it "has all products" do
            is_expected.to include(*products_all)
          end
        end

        context "when value presents" do
          let(:value) { "S" }
          let(:sort) { nil }

          it { is_expected.to eq [size_low] }
        end

        context "when sort presents" do
          let(:value) { nil }
          let(:sort) { "NEW_PRODUCT" }

          it "returns products from new to old " do
            expect(filtering_and_sorting.first).to eq red_new
            expect(filtering_and_sorting.last).to eq product_old
          end
        end

        context "when value and sort present" do
          let(:value) { "Red" }
          let(:sort) { "HIGH_PRICE" }

          it { is_expected.to eq [red_new] }
        end

        context "when there is a product with color and sort" do
          let!(:red_size) { create(:product) }
          let!(:variants) do
            [
              create(:variant, product: red_size, option_values: [value_red]),
              create(:variant, product: red_size, option_values: [value_size]),
            ]
          end
          let(:value) { "Red" }
          let(:sort) { nil }

          it "returns distinct products" do
            is_expected.to include red_size
            is_expected.to eq filtering_and_sorting.uniq
          end
        end
      end
    end

    describe "search_by" do
      subject { Spree::Product.search_by(word) }

      let(:product_a)    { create(:product, name: "Search") }
      let(:product_b)    { create(:product, description: "Search") }
      let(:product_meta) { create(:product, name: "%meta%") }

      context "searching for nomal word" do
        let(:word) { "sea" }

        it "returns products has word in name or descroption" do
          is_expected.to include(product_a, product_b)
          is_expected.not_to include(product_meta)
        end
      end

      context "searching for meta word" do
        let(:word) { "%" }

        it "returns products has meta word" do
          is_expected.to eq [product_meta]
        end
      end
    end
  end
end
