require 'rails_helper'

RSpec.describe Potepan::TaxonDecorator, type: :model do
  describe "model method" do
    let!(:taxon)      { create(:taxon) }
    let!(:categories) { create(:taxon, name: "categories") }
    let!(:bags)       { create(:taxon, name: "bags") }
    let!(:t_shirts)   { create(:taxon, name: "t-shirts") }

    context "three_popular_taxons" do
      subject { Spree::Taxon.three_popular_taxons.to_a }

      it "pickup three taxons order by name" do
        is_expected.to match_array [bags, categories, t_shirts]
        is_expected.not_to include taxon
      end
    end
  end
end
