require 'rails_helper'

RSpec.describe Potepan::OrderDecorator, type: :model do
  describe "model method" do
    let(:order) { create(:order, state: state) }

    describe "previous_state" do
      subject { order.previous_state }

      context "when state is address" do
        let(:state) { "address" }

        it { is_expected.to eq "cart" }
      end

      context "when state is address" do
        let(:state) { "payment" }

        it { is_expected.to eq "address" }
      end

      context "when state is address" do
        let(:state) { "confirm" }

        it { is_expected.to eq "payment" }
      end
    end

    describe "payment_status_design" do
      subject { order.payment_status_design }

      context "when state is address" do
        let(:state) { "address" }

        it { is_expected.to eq "disabled" }
      end

      context "when state is address" do
        let(:state) { "payment" }

        it { is_expected.to eq "active" }
      end

      context "when state is address" do
        let(:state) { "confirm" }

        it { is_expected.to eq "complete fullBar" }
      end
    end

    describe "pay_by_credit_card" do
      let(:state) { "address" }
      let!(:credit_card) do
        create(:payment_method, available_to_users: true, available_to_admin: true)
      end

      it "returns credit_card" do
        expect(order.pay_by_credit_card).to eq credit_card
      end
    end
  end
end
