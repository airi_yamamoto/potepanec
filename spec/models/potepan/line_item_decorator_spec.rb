require 'rails_helper'

RSpec.describe Potepan::LineItemDecorator, type: :model do
  describe "model_method" do
    describe "image_pickup" do
      let(:product_img) { create(:image) }
      let(:variant_img) { create(:image) }
      let!(:product)    { create(:product) }
      let!(:variant)    { create(:variant, product: product) }
      let!(:line_item)  { create(:line_item, variant: variant, product: product) }

      context "when no images" do
        it "returns Spree::Imge.new" do
          expect(line_item.image_pickup.url(:small)).to eq "noimage/small.png"
        end
      end

      context "when its variant has images" do
        it "returns variant_img" do
          variant.images << variant_img
          expect(line_item.image_pickup).to eq variant_img
        end
      end

      context "when its product has images" do
        it "returns variant_img" do
          product.images << product_img
          expect(line_item.image_pickup).to eq product_img
        end
      end
    end
  end
end
