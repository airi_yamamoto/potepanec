require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  let!(:taxon_a)        { create(:taxon) }
  let!(:taxon_b)        { create(:taxon) }
  let!(:product)        { create(:product, taxons: [taxon_a, taxon_b]) }
  let!(:product_search) { create_list(:product, 2, name: "search", taxons: [create(:taxon)]) }
  let!(:products) do
    create_list(:product, 2, taxons: [taxon_a, taxon_b])
    create_list(:product, 3, taxons: [taxon_b])
  end

  describe "GET #show" do
    before { get :show, params: { id: product.id } }

    describe "for response" do
      subject { response }

      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("products/show") }
    end

    describe "for instance variable" do
      it "assigns @product" do
        expect(assigns(:product)).to eq product
      end

      describe "assigns @related_products" do
        it "has 4 products" do
          expect(assigns(:related_products).count).to eq 4
        end

        it "has only related_products" do
          expect(product.related_products).to include(*assigns(:related_products))
        end
      end
    end
  end

  describe "GET #search" do
    let(:word) { "search" }

    before { get :search, params: { search: word } }

    describe "for response" do
      subject { response }

      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("products/search") }
    end

    describe "for instance variable" do
      it "assigns @search_word" do
        expect(assigns(:search_word)).to eq word
      end

      it "assigns @products" do
        expect(assigns(:products)).to eq product_search
      end
    end
  end
end
