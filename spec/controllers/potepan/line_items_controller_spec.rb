require 'rails_helper'

RSpec.describe Potepan::LineItemsController, type: :controller do
  describe "#destroy" do
    let!(:line_item) { create(:line_item) }
    let(:delete_destroy) { delete :destroy, params: { id: line_item.id } }

    it "delete the line_item" do
      expect { delete_destroy }.to change { Spree::LineItem.count }.by(-1)
    end

    it "redirect to referer" do
      from potepan_cart_path
      delete_destroy
      expect(response).to redirect_to potepan_cart_path
    end
  end
end
