require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET #show" do
    let!(:taxonomy)      { create(:taxonomy) }
    let!(:taxon)         { create(:taxon, taxonomy: taxonomy, products: products) }

    let!(:products)      { [*create_list(:product, 3), product_color, product_size] }
    let!(:product_color) { create(:product, variants: [variant_color]) }
    let!(:product_size)  { create(:product, variants: [variant_size]) }

    let!(:variant_color) { create(:variant, option_values: [value_color]) }
    let!(:variant_size)  { create(:variant, option_values: [value_size]) }

    let!(:type_color)    { create(:option_type, presentation: "Color") }
    let!(:value_color)   { create(:option_value, presentation: "Red", option_type: type_color) }
    let!(:type_size)     { create(:option_type, presentation: "Size") }
    let!(:value_size)    { create(:option_value, presentation: "S", option_type: type_size) }

    before { get :show, params: { id: taxon.id } }

    describe "for response" do
      subject { response }

      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("categories/show") }
    end

    describe "for instance variable" do
      it "assigns @category" do
        expect(assigns(:category)).to eq taxon
      end

      it "assigns @taxonomies" do
        expect(assigns(:taxonomies)).to eq Spree::Taxonomy.all
      end

      it "assigns @option_colors" do
        expect(assigns(:option_colors)).to eq [value_color]
      end

      it "assigns @option_sizes" do
        expect(assigns(:option_sizes)).to eq [value_size]
      end

      describe "assigns @products" do
        context "when there are no parameters" do
          it "returns all products associated taxon" do
            expect(assigns(:products)).to eq products
          end
        end

        context "with params[:color]" do
          before { get :show, params: { id: taxon.id, value: value_color.presentation } }

          it "returns a only product has color" do
            expect(assigns(:products)).to eq [product_color]
          end
        end

        context "with params[:size]" do
          before { get :show, params: { id: taxon.id, value: value_size.presentation } }

          it "returns a only product has size" do
            expect(assigns(:products)).to eq [product_size]
          end
        end

        context "with params[:sort]" do
          let!(:new_product)  { create(:product, available_on: Time.current, taxons: [taxon]) }
          let!(:old_product)  { create(:product, available_on: 5.year.ago, taxons: [taxon]) }
          let!(:high_product) { create(:product, price: "30.00", taxons: [taxon]) }
          let!(:low_product)  { create(:product, price: "10.00", taxons: [taxon]) }

          let!(:color_new) { create(:variant, product: new_product, option_values: [value_color]) }
          let!(:color_old) { create(:variant, product: old_product, option_values: [value_color]) }

          let!(:size_high) { create(:variant, product: high_product, option_values: [value_size]) }
          let!(:size_low)  { create(:variant, product: low_product, option_values: [value_size]) }

          describe "when params[:value] is not present" do
            before { get :show, params: { id: taxon.id, sort: sort } }

            context "when sort by available desc" do
              let(:sort) { "NEW_PRODUCT" }

              it "returns a product in order from new to old" do
                expect(assigns(:products).first).to eq new_product
                expect(assigns(:products).last).to eq old_product
              end
            end

            context "when sort by available asc" do
              let(:sort) { "OLD_PRODUCT" }

              it "returns a product in order from new to old" do
                expect(assigns(:products).first).to eq old_product
                expect(assigns(:products).last).to eq new_product
              end
            end

            context "when sort by price desc" do
              let(:sort) { "HIGH_PRICE" }

              it "returns a product in order from new to old" do
                expect(assigns(:products).first).to eq high_product
                expect(assigns(:products).last).to eq low_product
              end
            end

            context "when sort by price asc" do
              let(:sort) { "LOW_PRICE" }

              it "returns a product in order from new to old" do
                expect(assigns(:products).first).to eq low_product
                expect(assigns(:products).last).to eq high_product
              end
            end
          end

          describe "params[:value] is present" do
            context "when value == color" do
              context "when sort by available_desc" do
                before do
                  get :show, params: {
                    id: taxon.id,
                    sort: "NEW_PRODUCT",
                    value: value_color.presentation,
                  }
                end

                it "returns a product in order from new to old" do
                  expect(assigns(:products)).to eq [new_product, product_color, old_product]
                end
              end
            end

            context "when value == size" do
              context "when sort by price_desc" do
                before do
                  get :show, params: {
                    id: taxon.id,
                    sort: "HIGH_PRICE",
                    value: value_size.presentation,
                  }
                end

                it "returns a product in order from new to old" do
                  expect(assigns(:products)).to eq [high_product, product_size, low_product]
                end
              end
            end
          end
        end
      end
    end
  end
end
