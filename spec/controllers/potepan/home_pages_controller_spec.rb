require 'rails_helper'

RSpec.describe Potepan::HomePagesController, type: :controller do
  describe "GET #index" do
    subject { response }

    let!(:new_products)    { create_list(:product, 10, available_on: 1.week.ago) }
    let!(:old_products)    { create_list(:product, 5, available_on: 1.year.ago) }
    let!(:latest_product)  { create(:product, available_on: 1.day.ago) }
    let!(:feature_product) { create(:product, available_on: 1.day.from_now) }

    before { get :index }

    describe "for response" do
      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("home_pages/index") }
    end

    describe "for instance variable" do
      it "has 8 products" do
        expect(assigns(:new_products).count).to eq 8
      end

      it "returns latest first" do
        expect(assigns(:new_products).first).to eq latest_product
      end

      it "does not have old_products" do
        expect(assigns(:new_products)).not_to include(*old_products)
      end

      it "does not have feature_product" do
        expect(assigns(:new_products)).not_to include(feature_product)
      end
    end
  end
end
