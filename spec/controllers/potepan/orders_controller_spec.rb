require 'rails_helper'

RSpec.describe Potepan::OrdersController, type: :controller do
  subject { response }

  let(:token) { "token" }
  let!(:store) { create(:store) }

  describe "#show" do
    let!(:order) { create(:completed_order_with_totals, guest_token: token) }

    before do
      cookies.signed[:guest_token] = token
      get :show, params: { id: order.number }
    end

    describe "for response" do
      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("orders/show") }
    end

    describe "for instance variable " do
      it "assign @order" do
        expect(assigns(:order)).to eq order
      end
    end
  end

  describe "#edit" do
    let!(:order)     { create(:order, guest_token: token, line_items: [line_item]) }
    let!(:line_item) { create(:line_item) }

    before do
      cookies.signed[:guest_token] = token
      get :edit
    end

    describe "for response" do
      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("orders/edit") }
    end

    describe "for instance variable " do
      it "assign @order" do
        expect(assigns(:order)).to eq order
      end

      it "assign @line_items" do
        expect(assigns(:line_items)).to eq [line_item]
      end
    end
  end

  describe "#populate" do
    let!(:user)    { create(:user) }
    let!(:order)   { create(:order) }
    let!(:variant) { create(:variant) }

    let(:post_populate) { post :populate, params: { variant_id: variant.id, quantity: 2 } }

    before { allow(controller).to receive_messages(try_spree_current_user: user) }

    context "about orders" do
      context "current_order is nil" do
        it "creates user's new order" do
          expect { post_populate }.to change { user.orders.count }.by(1)
          expect(assigns(:order)).to eq user.orders.last
        end
      end

      context "current_order is present" do
        before { allow(controller).to receive_messages(current_order: order) }

        it "does not create new order" do
          expect { post_populate }.not_to change { user.orders.count }
          expect(assigns(:order)).to eq order
        end
      end
    end

    context "about line_items" do
      it "create line_item" do
        expect { post_populate } .to change { Spree::LineItem.count }.by(1)
        item = Spree::LineItem.last
        expect(item.variant).to eq variant
        expect(item.quantity).to eq 2
      end
    end

    describe "about other factors" do
      before { post_populate }

      it "has guest_token" do
        expect(cookies.signed[:guest_token]).not_to be_blank
      end

      it { is_expected.to redirect_to potepan_cart_path }
    end

    context "check params[:quantity]" do
      context "params[:quantity] is so many" do
        before { post :populate, params: { variant_id: variant.id, quantity: 1001 } }

        it "fails and return to potepan_root_path" do
          expect(flash[:error]).to be_present
          is_expected.to redirect_to potepan_root_path
        end
      end
    end
  end

  describe "#update" do
    describe "current_order is nil" do
      before do
        allow(controller).to receive_messages(current_order: nil)
        patch :update
      end

      it "redirect to root_path with error" do
        expect(flash[:error]).to be_present
        is_expected.to redirect_to potepan_root_path
      end
    end

    describe "current_order present" do
      let!(:order)    { create(:order_with_line_items, state: "cart", guest_token: token) }
      let(:line_item) { order.line_items.first }

      let(:update_params) do
        patch :update, params: {
          order: { line_items_attributes: { id: line_item.id, quantity: 2 } },
        }
      end

      let(:checkout_params) do
        patch :update, params: {
          checkout: "",
          order: { line_items_attributes: { id: line_item.id, quantity: 2 } },
        }
      end

      before do
        cookies.signed[:guest_token] = token
        allow(controller).to receive_messages(current_order: order)
      end

      context "when there is no params[:checkout]" do
        it "update the order and redirect_to potepan_cart_path" do
          expect { update_params }.to change(line_item, :quantity).from(1).to(2)
          is_expected.to redirect_to potepan_cart_path
        end
      end

      context "when params[:checkout] presents" do
        it "update order.state and redirect to potepan_checkout_state_path" do
          expect { checkout_params }.to change(order, :state).from("cart").to("address")
          is_expected.to redirect_to potepan_checkout_state_path(order.state)
        end
      end
    end
  end
end
