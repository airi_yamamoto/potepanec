require 'rails_helper'

RSpec.describe Potepan::CheckoutController, type: :controller do
  subject { response }

  let(:token)  { "token" }
  let!(:user)  { create(:user) }

  before do
    allow(controller).to receive_messages try_spree_current_user: user
    allow(controller).to receive_messages current_order: order
    cookies.signed[:guest_token] = token
  end

  describe "#edit" do
    let!(:order) { create(:order_with_totals, user: user, guest_token: token) }

    describe "for response" do
      before { get :edit, params: { state: "address" } }

      it { is_expected.to have_http_status(:success) }
      it { is_expected.to render_template("checkout/edit") }
    end

    describe "before_action" do
      let(:state) { "address" }
      let(:get_edit) { get :edit, params: { state: state } }

      it "assign @order" do
        get_edit
        expect(assigns(:order)).to eq order
      end

      it "set state if present" do
        allow(order).to receive_messages can_go_to_state?: true
        get_edit
        expect(assigns(:order).state).to eq state
      end

      it "redirect to potepan_cart_path unless checkout_allowed?" do
        allow(order).to receive_messages checkout_allowed?: false
        get_edit
        is_expected.to redirect_to potepan_cart_path
      end

      it "redirect to potepan_cart_path if order complete" do
        allow(order).to receive_messages completed?: true
        get_edit
        is_expected.to redirect_to potepan_cart_path
      end

      it "redirect to potepan_cart_path when insufficient stock lines" do
        allow(order).
          to receive_message_chain("insufficient_stock_line_items.present?").
          and_return(true)
        allow(order).
          to receive_message_chain("insufficient_stock_line_items.collect.to_sentence").
          and_return("item")

        get_edit
        is_expected.to redirect_to potepan_cart_path
      end

      it "redirect to potepan_cart_path when the params invalid" do
        allow(order).to receive_messages has_checkout_step?: false
        get_edit
        is_expected.to redirect_to potepan_cart_path
      end
    end
  end

  describe "#update" do
    describe "before_action" do
      let(:state)        { "address" }
      let(:patch_update) { patch :update, params: { state: state } }
      let!(:order) { create(:order_with_totals, user: user, guest_token: token) }

      it "assign @order" do
        patch_update
        expect(assigns(:order)).to eq order
      end

      it "set state if present" do
        allow(order).to receive_messages can_go_to_state?: true
        patch_update
        expect(assigns(:order).state).to eq state
      end

      it "redirect to potepan_cart_path unless checkout_allowed?" do
        allow(order).to receive_messages checkout_allowed?: false
        patch_update
        is_expected.to redirect_to potepan_cart_path
      end

      it "redirect to potepan_cart_path if order complete" do
        allow(order).to receive_messages completed?: true
        patch_update
        is_expected.to redirect_to potepan_cart_path
      end

      it "redirect to potepan_cart_path when insufficient stock lines" do
        allow(order).
          to receive_message_chain("insufficient_stock_line_items.present?").
          and_return(true)
        allow(order).
          to receive_message_chain("insufficient_stock_line_items.collect.to_sentence").
          and_return("item")

        patch_update
        is_expected.to redirect_to potepan_cart_path
      end

      it "redirect to potepan_cart_path when the params invalid" do
        allow(order).to receive_messages has_checkout_step?: false
        patch_update
        is_expected.to redirect_to potepan_cart_path
      end
    end

    describe "update order" do
      context "params[:state] is 'address'" do
        let(:address_params) do
          address = build(:address)
          address.attributes.except("created_at", "updated_at")
        end

        let(:patch_address) do
          patch :update, params: {
            state: "address",
            order: {
              ship_address_attributes: address_params,
              use_billing: true,
            },
          }
        end

        let!(:order) { create(:order_with_line_items, guest_token: token, state: "address") }

        context "send params" do
          it "changes the order state from address to payment" do
            expect { patch_address }.to change(order, :state).from("address").to("payment")
          end
        end
      end

      context "params[:state] is 'payment'" do
        let(:order)           { create(:order_with_line_items, guest_token: token) }
        let!(:payment_method) { create(:credit_card_payment_method) }

        let(:patch_payment) do
          patch :update, params: {
            state: "payment", order: {
              payments_attributes: [{
                payment_method_id: payment_method.id,
                source_attributes: attributes_for(:credit_card),
              }],
            },
          }
        end

        before { 3.times { order.next! } }

        context 'with a permitted payment method' do
          it 'sets the payment amount' do
            expect { patch_payment }.to change(order, :state).from("payment").to("confirm")
            expect(order.payments.size).to eq 1
            is_expected.to redirect_to potepan_checkout_state_path("confirm")
          end
        end
      end

      context "params[:state] is 'confirm'" do
        let!(:order)        { create(:order_ready_to_complete, guest_token: token) }
        let(:patch_confirm) { patch :update, params: { state: "confirm" } }

        it "change order state from confirm to complete" do
          expect { patch_confirm }.to change(order, :state).from("confirm").to("complete")
        end

        it "sends a correct confirm email by 1" do
          expect { patch_confirm }.to change { ActionMailer::Base.deliveries.count }.by(1)

          mail = ActionMailer::Base.deliveries.last

          expect(mail.from).to eq [order.store.mail_from_address]
          expect(mail.to).to eq [order.email]
          expect(mail.subject).to match order.number
        end

        context "send params" do
          before { patch_confirm }

          it { redirect_to potepan_order_path(order) }
        end
      end
    end
  end
end
