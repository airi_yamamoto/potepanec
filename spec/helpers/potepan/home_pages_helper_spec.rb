require 'rails_helper'

RSpec.describe Potepan::HomePagesHelper, type: :helper do
  describe "go_to_products_page" do
    context "when taxon is present" do
      let!(:taxon) { create(:taxon) }

      it "return potepan_category_path" do
        expect(helper.go_to_products_page).to eq potepan_category_path(Spree::Taxon.first.id)
      end
    end

    context "when taxon is not present" do
      it "return dummy link" do
        expect(helper.go_to_products_page).to eq "#"
      end
    end
  end
end
