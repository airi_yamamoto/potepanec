require 'rails_helper'

RSpec.describe Potepan::ProductsHelper, type: :helper do
  let!(:taxon)   { create(:taxon) }
  let!(:value)   { create(:option_value) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:others)  { create(:product, taxons: [create(:taxon)]) }
  let!(:variant) { create(:variant, product: product, option_values: [value]) }

  describe "products_count_for_option" do
    it "returns the products count associated with value and taxon" do
      expect(helper.products_count_for_option(value: value, category: taxon)).to eq 1
    end
  end
end
