require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "about full_title hepler" do
    let(:base_title) { "BIGBAG Store" }

    context "when page_title is not empty" do
      it "returns base_title with page_title" do
        expect(helper.full_title("HELLO")).to eq "HELLO - #{base_title}"
      end
    end

    context "when page_title is empty" do
      it "returns only base_title" do
        expect(helper.full_title).to eq base_title
      end
    end
  end
end
