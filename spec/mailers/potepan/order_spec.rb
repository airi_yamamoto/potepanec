require "rails_helper"

RSpec.describe Spree::OrderMailer, type: :mailer do
  describe "confirm_email" do
    let(:order)   { create(:completed_order_with_totals) }
    let(:store)   { order.store }
    let(:address) { order.ship_address }

    let(:mail)      { Potepan::OrderMailer.confirm_email(order) }
    let(:text_body) { get_message_part(mail, "plain") }
    let(:html_body) { get_message_part(mail, "html") }

    describe "mail header" do
      it "has corrext subject" do
        expect(mail.subject).to match("ご注文ありがとうございます！")
        expect(mail.subject).to match("注文番号:#{order.number}")
      end

      it "mail to customer email address" do
        expect(mail.to).to eq([order.email])
      end

      it "mail from store address" do
        expect(mail.from).to eq([store.mail_from_address])
      end
    end

    describe "mail body" do
      context "about html mail" do
        subject { html_body }

        it "has thanks massage" do
          expect_to_have_thanks_message
        end

        it "has ship address information" do
          expect_to_have_address_information
        end

        it "has order information" do
          expect_to_have_order_information
        end

        it "has note order is fake" do
          expect_to_have_note
        end
      end

      context "about text mail" do
        subject { text_body }

        it "has thanks massage" do
          expect_to_have_thanks_message
        end

        it "has ship address information" do
          expect_to_have_address_information
        end

        it "has order information" do
          expect_to_have_order_information
        end

        it "has note order is fake" do
          expect_to_have_note
        end
      end
    end
  end

  def expect_to_have_thanks_message
    is_expected.to match "ありがとう"
  end

  def expect_to_have_address_information
    aggregate_failures do
      is_expected.to match address.full_name
      is_expected.to match address.zipcode
      is_expected.to match address.address1
    end
  end

  def expect_to_have_order_information
    aggregate_failures do
      is_expected.to match store.name
      is_expected.to match order.number
      is_expected.to have_content order.display_total
      order.line_items.each do |line_item|
        is_expected.to match line_item.name
      end
    end
  end

  def expect_to_have_note
    aggregate_failures do
      is_expected.to match "購入処理はテストモード"
      is_expected.to match "料金のお引き落としが行われることはございません"
    end
  end
end
