require 'rails_helper'

RSpec.describe "Headers", type: :feature do
  subject { page }

  before { visit potepan_root_url }

  describe "header layouts" do
    it { is_expected.to have_link "Home", href: potepan_root_url }
    it { is_expected.to have_link "Products" }

    it "has a logo links to potepan_root_url" do
      link = find(".navbar-brand")
      expect(link[:href]).to eq potepan_root_url
    end

    it "has a link to potepan_cart_path" do
      link = find(".cart_link")
      expect(link[:href]).to eq potepan_cart_path
    end
  end

  describe "search products" do
    let!(:product_a) { create(:product, name: "a_product") }
    let!(:product_b) { create(:product, name: "b_product") }

    it "has a form for searching products" do
      find(".searchForm").click

      fill_in "search", with: "a_"
      click_on "検索"

      expect(current_path).to eq search_potepan_products_path

      within first(".productBox") do
        is_expected.to have_link product_a.name
      end
      is_expected.to have_no_link product_b.name
    end
  end
end
