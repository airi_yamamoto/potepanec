require 'rails_helper'

RSpec.feature "Order", type: :feature do
  include ApplicationHelper
  subject { page }

  let!(:user)    { create(:user) }
  let!(:store)   { create(:store) }
  let!(:taxon)   { create(:taxon, products: [product]) }
  let!(:product) { create(:product) }
  let!(:country) { create(:country, states_required: true, name: "Japan") }
  let!(:state)   { create(:state, country: country) }

  before do
    allow_any_instance_of(Potepan::OrdersController).
      to receive_messages(try_spree_current_user: user)

    allow_any_instance_of(Potepan::CheckoutController).
      to receive_messages(try_spree_current_user: user)
  end

  describe "order has no line item" do
    before { visit potepan_cart_path }

    it "show cart empty" do
      is_expected.to have_content "カートは空です"
      is_expected.to have_no_link "次へ"
    end
  end

  describe "add line_item to cart" do
    let(:order)      { user.orders.last }
    let(:line_item)  { user.orders.last.line_items.last }
    let(:line_items) { user.orders.last.line_items }

    before { add_cart_to_product }

    context "check the page layout" do
      it { is_expected.to have_title full_title("Cart") }

      it "redirect to potepan_cart_path" do
        expect(current_path).to eq potepan_cart_path
      end
      it "has line_item details" do
        within ".table" do
          is_expected.to have_link nil, href: potepan_line_item_path(line_item.id)
          is_expected.to have_link product.name
          is_expected.to have_content product.display_price
          is_expected.to have_field "order_line_items_attributes_0_quantity", with: 2
          is_expected.to have_content "$#{2 * product.price.to_f}"
          line_items.each do |line_item|
            line_item.display_amount
          end
        end
      end

      it "has order checkout amount" do
        within ".totalAmountArea" do
          is_expected.to have_content order.display_item_total
          is_expected.to have_content order.display_tax_total
          is_expected.to have_content order.display_total
        end
      end
    end

    context "has some button" do
      context "click delete button" do
        it "delete order line_item" do
          is_expected.to have_link product.name

          find(".line_item_delete").click

          expect(current_path).to eq potepan_cart_path

          is_expected.to have_no_link product.name
          is_expected.to have_content "カートは空です"
        end
      end

      context "push button 'アップデート'" do
        it "change the order details" do
          fill_in "order_line_items_attributes_0_quantity", with: 3
          click_button "アップデート"

          expect(current_path).to eq potepan_cart_path

          is_expected.to have_field "order_line_items_attributes_0_quantity", with: 3
          within first(".line_item_amount") do
            is_expected.to have_content "$#{3 * product.price.to_f}"
          end
        end
      end

      context "push button '購入する'" do
        it "redirect to potepan_checkout_state_path(address)" do
          click_on "購入する"
          expect(current_path).to eq potepan_checkout_state_path("address")
        end
      end
    end

    context "when quantity is so many" do
      it "redirect back with flash_message" do
        fill_in "order_line_items_attributes_0_quantity", with: 10000000000000000
        click_button "アップデート"

        expect(current_path).to eq potepan_cart_path
        within ".flash_message" do
          is_expected.to have_selector ".alert-error"
        end
      end
    end
  end

  def add_cart_to_product
    visit potepan_product_path(product.id)
    select "2", from: "quantity"
    click_button "カートへ入れる"
  end
end
