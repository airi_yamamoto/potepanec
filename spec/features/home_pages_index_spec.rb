require 'rails_helper'

RSpec.feature "HomePagesIndex", type: :feature do
  subject { page }

  let!(:taxon)          { create(:taxon) }
  let!(:categories)     { create(:taxon, name: "categories") }
  let!(:bags)           { create(:taxon, name: "bags") }
  let!(:t_shirts)       { create(:taxon, name: "t-shirts") }

  let!(:latest_product) { create(:product, name: "NEW", available_on: 1.day.ago, taxons: [taxon]) }
  let!(:future_product) { create(:product, name: "FUTURE", available_on: 1.day.from_now) }
  let!(:new_paroducts)  { create_list(:product, 10, name: "NEW", available_on: 1.week.ago) }
  let!(:old_paroducts)  { create_list(:product, 10, name: "OLD", available_on: 1.year.ago) }

  before { visit potepan_root_path }

  describe "check home_pages#index layout" do
    it "has 8 only new_products" do
      within(".featuredProductsSlider") do
        is_expected.to have_link "NEW", count: 8
        is_expected.to have_no_link "OLD"
        is_expected.to have_no_link "FUTURE"
      end
    end

    it "has link to new_products pages" do
      first(".productBox").click_on "NEW-img"
      expect(current_path).to eq potepan_product_path(latest_product.id)
    end

    it "has links for popular categories " do
      is_expected.to have_link categories.name, href: potepan_category_path(categories.id)
      is_expected.to have_link bags.name, href: potepan_category_path(bags.id)
      is_expected.to have_link t_shirts.name, href: potepan_category_path(t_shirts.id)
    end

    it "has no links for unpopular categories" do
      is_expected.to have_no_link taxon.name, href: potepan_category_path(taxon.id)
    end
  end
end
