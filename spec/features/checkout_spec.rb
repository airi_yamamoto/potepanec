require 'rails_helper'

RSpec.feature "Checkout", type: :feature do
  include ApplicationHelper

  subject { page }

  let(:order) { user.orders.last }

  let!(:store)   { create(:store) }
  let!(:user)    { create(:user) }
  let!(:country) { create(:country, states_required: true, name: "Japan") }
  let!(:state)   { create(:state, country: country, name: "Tokyo") }

  let!(:taxon)   { create(:taxon, products: [product]) }
  let!(:product) { create(:product) }

  let!(:credit_card)     { create(:credit_card_payment_method) }
  let!(:zone)            { create(:zone) }
  let!(:shipping_method) { create(:shipping_method) }
  let!(:stock_location)  { create(:stock_location) }

  before do
    allow_any_instance_of(Potepan::OrdersController).
      to receive_messages(try_spree_current_user: user)

    allow_any_instance_of(Potepan::CheckoutController).
      to receive_messages(try_spree_current_user: user)
  end

  describe "checkout successful" do
    it "checkout process" do
      add_product_to_cart_and_go_to_checkout

      expect(current_path).to eq potepan_checkout_state_path("address")
      is_expected.to have_title full_title("Checkout")

      # display error when submit with invalid address
      fill_in_address_with_invalid_information
      click_on "次へ"

      is_expected.to have_css "#error_explanation"
      expect_to_display_error_message(content: "The form contains 5 errors.")

      # redirect to payment page when submit with valid address
      fill_in_address_with_valid_information
      click_on "次へ"
      expect(current_path).to eq potepan_checkout_state_path("payment")

      # display error when submit with invalid credit card
      expect_to_have_hidden_fields
      fill_in_invalid_credit_card
      click_on "次へ"

      is_expected.to have_css "#error_explanation"
      expect_to_display_error_message(content: "The form contains 3 errors.")

      # when submit with bogus credit card and confirm checkout
      fill_in_credit_card(number: "123")
      click_on "次へ"
      expect(current_path).to eq potepan_checkout_state_path("confirm")
      click_on "購入確定"

      # Spree::Core::GatewayError occurs
      expect(current_path).to eq potepan_checkout_state_path("payment")
      expect_to_display_error_message(content: "Bogus Gateway: Forced failure")

      # when submit with valid credit card
      fill_in_credit_card(number: "4111 1111 1111 1111")
      click_on "次へ"

      # display user address in confirm page
      expect(current_path).to eq potepan_checkout_state_path("confirm")
      expect_to_display_user_address
      click_on "購入確定"

      # redirect to order#show and send a confirm_email
      expect(current_path).to eq potepan_order_path(order.number)
      is_expected.to have_title full_title("Thanks!")
      is_expected.to have_content "ご注文ありがとうございます。"
      is_expected.to have_content order.number
      expect_to_display_user_address
      expect_to_send_confirm_email
    end
  end

  def add_product_to_cart_and_go_to_checkout
    visit potepan_product_path(product.id)
    select "1", from: "quantity"
    click_button "カートへ入れる"
    click_on "購入する"
  end

  def fill_in_address_with_invalid_information
    fill_in "order_email", with: ""

    address = "order_ship_address_attributes"
    fill_in "#{address}_firstname", with: ""
    fill_in "#{address}_lastname",  with: ""
    fill_in "#{address}_zipcode",   with: ""
    fill_in "#{address}_city",      with: ""
    fill_in "#{address}_address1",  with: ""
    fill_in "#{address}_phone",     with: ""
    select "Tokyo", from: "#{address}_state_id"
  end

  def fill_in_address_with_valid_information
    fill_in "order_email", with: "user@example.com"

    address = "order_ship_address_attributes"
    fill_in "#{address}_firstname", with: "first"
    fill_in "#{address}_lastname",  with: "last"
    fill_in "#{address}_zipcode",   with: "1234567"
    fill_in "#{address}_city",      with: "city"
    fill_in "#{address}_address1",  with: "address1"
    fill_in "#{address}_phone",     with: "09012345678"
    select "Tokyo", from: "#{address}_state_id"
  end

  def expect_to_have_hidden_fields
    payment_method = find("#setting_payment_method", visible: false)
    expect(payment_method.value).to eq credit_card.id.to_s

    cc_type = find("#setting_cc_type", visible: false)
    expect(cc_type.value).to eq ""
  end

  def fill_in_invalid_credit_card
    payment = "payment_source_#{credit_card.id}"
    fill_in "#{payment}_name", with: ""
    fill_in "#{payment}_number", with: ""
    fill_in "#{payment}_verification_value", with: ""
  end

  def fill_in_credit_card(number: "")
    payment = "payment_source_#{credit_card.id}"
    fill_in "#{payment}_name", with: 'name'
    fill_in "#{payment}_number", with: number
    select "1", from: "payment_source_#{credit_card.id}_month"
    select "#{Time.current.year}", from: "payment_source_#{credit_card.id}_year"
    fill_in "#{payment}_verification_value", with: "123"
  end

  def expect_to_display_user_address
    aggregate_failures do
      within "address" do
        within ".address_details" do
          is_expected.to have_content "first last"
          is_expected.to have_content "〒 1234567"
          is_expected.to have_content "city"
          is_expected.to have_content "address1"
        end
        is_expected.to have_content "09012345678"
        is_expected.to have_content "user@example.com"
      end
    end
  end

  def expect_to_display_error_message(content:)
    within ".alert" do
      is_expected.to have_content content
    end
  end

  def expect_to_send_confirm_email
    mail = ActionMailer::Base.deliveries.last
    expect(mail.from).to eq [order.store.mail_from_address]
    expect(mail.to).to eq [order.email]
    expect(mail.subject).to match order.number
  end
end
