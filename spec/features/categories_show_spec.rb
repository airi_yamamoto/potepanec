require 'rails_helper'

RSpec.feature "CategoriesShows", type: :feature do
  include ApplicationHelper
  subject { page }

  let!(:taxonomy)    { create(:taxonomy, name: "Categories") }

  let!(:taxon)       { create(:taxon, name: "Bags", taxonomy: taxonomy, products: products) }
  let!(:sub_taxon)   { create(:taxon, name: "Others", taxonomy: taxonomy, products: [sub_product]) }
  let!(:empty_taxon) { create(:taxon, name: "Empty", taxonomy: taxonomy) }

  let!(:products)     { [*create_list(:product, 2), product_high, product_red, product_s] }
  let!(:product_high) { create(:product, name: "High", price: "25.00") }
  let!(:product_red)  { create(:product, variants: [variant_red]) }
  let!(:product_s)    { create(:product, variants: [variant_s]) }
  let!(:sub_product)  { create(:product) }

  let!(:variant_red) { create(:variant, option_values: [value_color]) }
  let!(:variant_s)   { create(:variant, option_values: [value_size]) }

  let!(:type_color)  { create(:option_type, presentation: "Color") }
  let!(:type_size)   { create(:option_type, presentation: "Size") }
  let!(:value_color) { create(:option_value, presentation: "Red", option_type: type_color) }
  let!(:value_size)  { create(:option_value, presentation: "S", option_type: type_size) }

  before { visit potepan_category_path(taxon.id) }

  describe "check categories/show layout" do
    it { is_expected.to have_title full_title("Bags") }

    it "has a correct page_header" do
      within ".pageHeader" do
        within(".page-title") { is_expected.to have_content "Bags" }
        within(".active") { is_expected.to have_content "Bags" }
        is_expected.to have_link "Home", href: potepan_root_url
      end
    end

    context "about side_bar" do
      it "has a categories_menu with association products count" do
        within ".sideBar" do
          within first(".panel-body") do
            is_expected.to have_content "Categories"
            within("#taxonomy-id-#{taxonomy.id}") do
              is_expected.to have_link "Bags(5)", href: potepan_category_path(taxon.id)
              is_expected.to have_link "Others(1)", href: potepan_category_path(sub_taxon.id)
              is_expected.not_to have_link "Empty(0)", href: potepan_category_path(empty_taxon.id)
            end
          end
        end
      end

      it "has a menu to filter by color with associated products count" do
        within ".filterByColor" do
          is_expected.to have_link "Red (1)", href: potepan_category_path(taxon.id, value: "Red")
        end
      end

      it "has a menu to filter by size with associated products count" do
        within ".filterBySize" do
          is_expected.to have_link "S (1)", href: potepan_category_path(taxon.id, value: "S")
        end
      end
    end

    context "about filter_area" do
      it "has correct select-drop for sorting" do
        within ".filterArea" do
          is_expected.to have_select(
            'sort',
            selected: "新着順",
            options: ["新着順", "古い順", "安い順", "高い順"]
          )
        end
      end
    end

    context "about product_boxes" do
      it "has related products informations" do
        products.each do |product|
          within "#product_#{product.id}_link" do
            is_expected.to have_content product.name
            is_expected.to have_content product.price
          end
        end
      end

      it { is_expected.to have_no_link sub_product.name }
    end
  end

  describe "check that links work correctly" do
    context "when click product_img" do
      it "has a link to potepan_product_path" do
        first_product = products.first

        click_on "#{first_product.name}-img"
        expect(current_path).to eq potepan_product_path(first_product.id)

        click_on "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end
    end

    context "when click link Red (1)" do
      it "has only product_red" do
        click_link "Red (1)"

        within(".productBox") { is_expected.to have_link product_red.name }

        no_colors = products.reject { |product| product == product_red }

        no_colors.each do |product|
          is_expected.to have_no_link product.name
        end
      end
    end

    context "when click link S (1)" do
      it "has only product_s" do
        click_link "S (1)"

        within(".productBox") { is_expected.to have_link product_s.name }

        no_sizes = products.reject { |product| product == product_s }

        no_sizes.each do |product|
          is_expected.to have_no_link product.name
        end
      end
    end

    context "when click option of sort" do
      it "return products from high_price to low_price" do
        select("高い順", from: "sort")
        is_expected.to have_select('sort', selected: '高い順')
      end
    end
  end
end
